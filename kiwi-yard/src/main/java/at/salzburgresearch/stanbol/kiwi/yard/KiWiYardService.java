package at.salzburgresearch.stanbol.kiwi.yard;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.ConfigurationPolicy;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.stanbol.entityhub.servicesapi.yard.Yard;
import org.apache.stanbol.entityhub.yard.sesame.SesameYard;
import org.apache.stanbol.entityhub.yard.sesame.SesameYardConfig;
import org.openrdf.repository.RepositoryException;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Dictionary;
import java.util.Hashtable;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
@Component(
        configurationFactory=true,
        policy= ConfigurationPolicy.REQUIRE, //the ID is required!
        specVersion="1.1",
        metatype = true,
        immediate = true,
        label = "Apache Stanbol: KiWi Yard"
)
@Service(KiWiYardService.class)
@Properties(value={
        @Property(name=Yard.ID,value="kiwi-yard"),
        @Property(name=Yard.NAME,value="KiWi Yard"),
        @Property(name=Yard.DESCRIPTION,value="A yard implementation for accessing a KiWi triplestore; requires a KiWiRepositoryService")
})
public class KiWiYardService implements ServiceTrackerCustomizer {

    private static Logger log = LoggerFactory.getLogger(KiWiYardService.class);

    @Property(label = "Context URI", description = "URI of the context (named graph) to use for querying")
    public static final String CONTEXT = "at.salzburgresearch.stanbol.kiwi.context";


    @Property(label = "Repository", description = "ID of the repository service to use as underlying triple store")
    public static final String REPOSITORY_ID = "at.salzburgresearch.stanbol.kiwi.repository";

    private ServiceTracker repositoryTracker;

    private BundleContext bundleContext;

    private ServiceRegistration yardService;

    private Dictionary<String,Object> config;

    /**
     * Register a service tracker for the KiWiRepositoryService; once it is there, register a new SesameYard.
     *
     * @param context
     * @throws ConfigurationException
     * @throws RepositoryException
     */
    @Activate
    protected final void activate(ComponentContext context) throws ConfigurationException, RepositoryException {
        this.config = context.getProperties();

        String filter = String.format("(&(objectClass=%s)(%s=%s))", KiWiRepositoryService.class.getName(),KiWiRepositoryService.REP_ID, config.get(REPOSITORY_ID));

        this.bundleContext = context.getBundleContext();

        try {
            repositoryTracker = new ServiceTracker(context.getBundleContext(),context.getBundleContext().createFilter(filter), this);
            repositoryTracker.open();
        } catch (InvalidSyntaxException e) {
            log.error("could not initialise service tracker for relatedness service (syntax error in query)");
        }
    }

    @Deactivate
    protected final void deactivate(ComponentContext context) throws RepositoryException {
        if(yardService != null) {
            yardService.unregister();
        }
        if(repositoryTracker != null) {
            repositoryTracker.close();
        }
    }


    @Override
    public Object addingService(ServiceReference serviceReference) {
        KiWiRepositoryService repositoryService = (KiWiRepositoryService) bundleContext.getService(serviceReference);

        if(repositoryService.getRepository() == null) {
            log.error("could not configure KiWi Yard: repository not available");
        } else {

            try {
                Hashtable<String,Object> yardProperties = new Hashtable<String, Object>();
                yardProperties.put(Yard.ID, this.config.get(Yard.ID));
                yardProperties.put(Yard.NAME, this.config.get(Yard.NAME));
                yardProperties.put(Yard.DESCRIPTION, this.config.get(Yard.DESCRIPTION));
                SesameYardConfig yardConfig = new SesameYardConfig(yardProperties);

                yardConfig.setContextEnabled(true);
                yardConfig.setContexts(new String[] {(String) this.config.get(CONTEXT)});
                yardConfig.setMaxQueryResultNumber(100);

                SesameYard yard = new SesameYard(repositoryService.getRepository(),yardConfig);

                yardService = bundleContext.registerService(Yard.class.getName(), yard, yardConfig.getDictionary());
            } catch (ConfigurationException ex) {
                log.error("could not configure KiWi Yard",ex);
            }
        }

        return repositoryService;
    }

    @Override
    public void modifiedService(ServiceReference serviceReference, Object o) {

    }

    @Override
    public void removedService(ServiceReference serviceReference, Object o) {
        bundleContext.ungetService(serviceReference);
    }
}
