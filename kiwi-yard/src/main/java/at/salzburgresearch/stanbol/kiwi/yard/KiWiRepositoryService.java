package at.salzburgresearch.stanbol.kiwi.yard;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.ConfigurationPolicy;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.marmotta.kiwi.config.KiWiConfiguration;
import org.apache.marmotta.kiwi.persistence.KiWiDialect;
import org.apache.marmotta.kiwi.persistence.h2.H2Dialect;
import org.apache.marmotta.kiwi.persistence.mysql.MySQLDialect;
import org.apache.marmotta.kiwi.persistence.pgsql.PostgreSQLDialect;
import org.apache.marmotta.kiwi.sail.KiWiStore;
import org.apache.marmotta.kiwi.sparql.sail.KiWiSparqlSail;
import org.apache.stanbol.entityhub.servicesapi.yard.Yard;
import org.openrdf.model.URI;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.contextaware.ContextAwareConnection;
import org.openrdf.repository.sail.SailRepository;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
@Component(
        configurationFactory=true,
        policy= ConfigurationPolicy.REQUIRE, //the ID is required!
        specVersion="1.1",
        metatype = true,
        immediate = true,
        label = "Apache Stanbol: KiWi Repository"
)
@Service(value = KiWiRepositoryService.class)
public class KiWiRepositoryService {

    private static Logger log = LoggerFactory.getLogger(KiWiRepositoryService.class);

    @Property(label = "Repository ID", description = "unique identifier for the repository service", value = "kiwi-repository")
    public static final String REP_ID = "at.salzburgresearch.stanbol.kiwi.name";

    @Property(label = "DB Dialect", description = "database dialect to use (default = postgres)", value = "postgres")
    public static final String DB_DIALECT = "at.salzburgresearch.stanbol.kiwi.dialect";

    @Property(label = "DB Host", description = "database host name where the database is located", value = "localhost")
    public static final String DB_HOST = "at.salzburgresearch.stanbol.kiwi.host";

    @Property(label = "DB Port", description = "database port number where the database is located", intValue = 5432)
    public static final String DB_PORT = "at.salzburgresearch.stanbol.kiwi.port";

    @Property(label = "DB Name", description = "database name to use for accessing the database", value = "lmf")
    public static final String DB_NAME = "at.salzburgresearch.stanbol.kiwi.database";

    @Property(label = "DB User", description = "database user name to use for accessing the database", value = "lmf")
    public static final String DB_USER = "at.salzburgresearch.stanbol.kiwi.user";

    @Property(label = "DB Password", description = "database password to use for accessing the database", value = "lmf")
    public static final String DB_PASS = "at.salzburgresearch.stanbol.kiwi.password";

    @Property(label = "DB Options", description = "database options to use for accessing the database")
    public static final String DB_OPTS = "at.salzburgresearch.stanbol.kiwi.options";

    @Property(label = "Pool Size", description = "database connection pool size to use (default = 20)", intValue = 20)
    public static final String DB_POOL = "at.salzburgresearch.stanbol.kiwi.pool_size";



    private Repository repository;

    @Activate
    protected final void activate(ComponentContext context) throws ConfigurationException, RepositoryException {
        log.info("starting KiWi repository service ...");
        if(context == null || context.getProperties() == null){
            throw new IllegalStateException("No valid"+ComponentContext.class+" parsed in activate!");
        }

        String db_type;
        KiWiDialect dialect;
        if(StringUtils.equalsIgnoreCase("postgres",(String)context.getProperties().get(DB_DIALECT))) {
            dialect = new PostgreSQLDialect();
            db_type = "postgresql";
        } else if(StringUtils.equalsIgnoreCase("mysql",(String)context.getProperties().get(DB_DIALECT))) {
            dialect = new MySQLDialect();
            db_type = "mysql";
        } else if(StringUtils.equalsIgnoreCase("h2",(String)context.getProperties().get(DB_DIALECT))) {
            dialect = new H2Dialect();
            db_type = "h2";
        } else {
            throw new IllegalStateException("no valid dialect was given");
        }

        String name = (String)context.getProperties().get(Yard.ID);
        String db_host = (String)context.getProperties().get(DB_HOST);
        String db_name = (String)context.getProperties().get(DB_NAME);
        String db_user = (String)context.getProperties().get(DB_USER);
        String db_pass = (String)context.getProperties().get(DB_PASS);
        int    db_port = (Integer)context.getProperties().get(DB_PORT);
        String db_opts = (String)context.getProperties().get(DB_OPTS);

        String url = String.format("jdbc:%s://%s:%d/%s%s", db_type, db_host, db_port, db_name, db_opts);

        log.debug("KiWi repository: database URL is {}", url);

        KiWiConfiguration configuration = new KiWiConfiguration(name,url,db_user,db_pass,dialect);
        KiWiStore store = new KiWiStore(configuration);
        repository = new SailRepository(new KiWiSparqlSail(store));
        repository.initialize();
    }


    @Deactivate
    protected final void deactivate(ComponentContext context) throws RepositoryException {
        repository.shutDown();
    }


    /**
     * Return a repository connection restricted to the given context.
     *
     * @param context
     * @return
     * @throws RepositoryException
     */
    public RepositoryConnection getConnection(String context) throws RepositoryException {
        URI ctx = repository.getValueFactory().createURI(context);

        ContextAwareConnection con = new ContextAwareConnection(repository, repository.getConnection());
        con.setInsertContext(ctx);
        con.setReadContexts(ctx);
        con.setRemoveContexts(ctx);

        return con;
    }

    /**
     * Directly return the repository managed by this service. Should be used with care, since all contexts
     * will be accessible.
     *
     * @return
     */
    public Repository getRepository() {
        return repository;
    }
}
