package at.salzburgresearch.stanbol.kiwi.site;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.ConfigurationPolicy;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.stanbol.entityhub.servicesapi.site.Site;
import org.apache.stanbol.entityhub.servicesapi.yard.Yard;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
@Component(
        configurationFactory=true,
        policy= ConfigurationPolicy.REQUIRE, //the ID is required!
        specVersion="1.1",
        metatype = true
)
@Service
@Properties(value={
        @Property(name= Yard.ID,value="changeme"),
        @Property(name=Yard.NAME,value="KiWi Site"),
        @Property(name=Yard.DESCRIPTION,value="A site implementation for accessing a KiWi triplestore"),
 })
public class KiWiSite  {


}
